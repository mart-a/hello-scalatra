#use ubuntu 
FROM tomcat:7.0
#add maintainer
MAINTAINER Martin A
copy target/*.war /usr/local/tomcat/webapps/hello-scalatra.war

EXPOSE 8080

CMD ["catalina.sh", "run"]
